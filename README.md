# wordcloud-webapp

É um projeto de software livre que tem por objetivos:

+ Disponibilizar uma interface web para criar wordclouds

### Instruções de uso

Para executar o projeto, é preciso ter um ambiente configurado com
- [Docker](https://docs.docker.com/)
- [Docker-Compose](https://docs.docker.com/compose/)

#### Comandos iniciais

Após clonar o repositório do projeto, navegue até a pasta do download

```
$ cd wordcloud-webapp
```

Em seguida, execute o seguinte comando para iniciar a aplicação

```
$ docker-compose up -d
```

Para verificar se a aplicação está respondendo, acesse
[http://localhost:1003/]