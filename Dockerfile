FROM alpine:3.14

# Add user for laravel application
RUN adduser -D -g 'mrbot' mrbot
RUN mkdir /home/mrbot/app

# Set working directory
WORKDIR /home/mrbot/app

RUN apk update
RUN apk add tzdata
RUN apk add supervisor
RUN apk add python3
RUN apk add py3-pip

# config TZ
ENV TIMEZONE=America/Sao_Paulo
RUN cp /usr/share/zoneinfo/${TIMEZONE} /etc/localtime
RUN echo "${TIMEZONE}" > /etc/timezone

# Set working directory
WORKDIR /home/mrbot/app

# Copy requirements.txt and app src
COPY app/ /home/mrbot/app/

# Copy config files
COPY ./supervisord.conf /etc/supervisord.conf

RUN export FLASK_APP=app.py 

RUN apk add py3-numpy py3-wheel py3-pillow py3-matplotlib python3-dev gcc build-base

# install dependencies
RUN pip install -r requirements.txt

# Run a cron job
ADD ./crontab.txt /crontab.txt
RUN /usr/bin/crontab /crontab.txt

# Expose port 5000 and start gunicorn server
EXPOSE 5000
ENTRYPOINT /usr/bin/supervisord -c /etc/supervisord.conf