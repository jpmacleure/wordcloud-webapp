# pylint: disable=import-error
import os
from flask import Flask, render_template, url_for, request, redirect
from flask import request
import wikipedia
from wordcloud import WordCloud
import nltk
import random
import string
import numpy as np
from PIL import Image
import glob

app = Flask(__name__)

# download stopwords
nltk.download("stopwords") 

# Gunicorn reference to app
application = app

@app.route('/')
def index():
    return render_template('index.html')

@app.route('/nuvem', methods=["POST"])
def nuvem():
    if request.method == "POST":
        words = request.form["words"]
        count = request.form["count"]
        height = request.form["height"]
        width = request.form["width"]
        minfontsize = request.form["minfontsize"]
        maxfontsize = request.form["maxfontsize"]
    
        count = int(count)
        width = int(width)
        height = int(height)
        minfontsize = int(minfontsize)
        maxfontsize = int(maxfontsize)
        
        wikipedia.set_lang("pt")
     
        title = wikipedia.search(words)[0]
        try:
            page = wikipedia.page(title)
        except wikipedia.DisambiguationError as e:
            s = e.options
            page = wikipedia.page(s)

        #print(page.content)
        nltk_stopwords = nltk.corpus.stopwords.words('portuguese')

        mask = np.array(Image.open("static/images/cloud-vector.png"))
        mask[mask == 0] = 255
        wc = WordCloud(background_color="white",
                    max_words=count,
                    width = width,
                    min_font_size = minfontsize,
                    max_font_size = maxfontsize,
                    height = height,
                    stopwords = nltk_stopwords,
                    mask=mask)
        
        wc.generate(page.content)
        random_string = get_random_string(10)
        wc.to_file("static/images/tmp/"+random_string+".png")
        return render_template("show.html", imagem_nuvem="static/images/tmp/"+random_string+".png", wordcloud_png=random_string)

@app.route('/show', methods=["GET"])
def nuvem_show():
    wordcloud_png = request.args.get('n')
    return render_template("show.html", imagem_nuvem="static/images/tmp/"+wordcloud_png+".png", wordcloud_png=wordcloud_png)

@app.route('/clear')
def clear():
    files = glob.glob('static/images/tmp/*.png')
    for f in files:
        try:
            os.remove(f)
        except OSError as e:
            print("Error: %s : %s" % (f, e.strerror))
    return render_template('index.html')

def get_random_string(length):
    letters = string.ascii_lowercase
    result_str = ''.join(random.choice(letters) for i in range(length))
    return result_str



if __name__ == '__main__':
    app.run()