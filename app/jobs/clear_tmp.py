import os
import glob

def clear_img():
    files = glob.glob('/home/mrbot/app/static/images/tmp/*.png')
    for f in files:
        try:
            os.remove(f)
        except OSError as e:
            print("Error: %s : %s" % (f, e.strerror))

clear_img()