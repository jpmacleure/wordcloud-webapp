<!-- 

### Added - para features adicionadas
### Removed - para features removidas
### Fixed - para correções no código
### Changed - para features modificadas

-->

# Release Notes

## [Unreleased]

## [0.0.12] - 06/04/2022
### Fixed
- Corrige link whatsapp

## [0.0.11] - 06/04/2022
### Added
- Adiciona /clear
- Adiciona job clear_img
- Adiciona crontab.txt

## [0.0.10] - 06/04/2022
### Added
- Adiciona botão compartilhar whatsapp
### Changed
- Modifica/simplifica interface

## [0.0.9] - 05/04/2022
### Changed
- Atualiza versão flask para 2.1.0
- Atualiza versão alpine para 3.14

## [0.0.8] - 21/03/2021
### Changed
- Melhora oadrão de cores da interface
- Atualiza copyright rodapé

## [0.0.7] - 03/10/2020
### Changed
- Melhora interface, add cores no cabeçalho e no rodapé, add copyright rodapé

## [0.0.6] - 02/10/2020
### Changed
- Melhora interface, add cores, limita tamanho da imagem wordcloud

## [0.0.5] - 01/10/2020
### Changed
- Modifica gitignore /images/*

## [0.0.4] - 30/09/2020
### Fixed 
 - adiciona a pasta images 
 - Corrige o erro [DisambiguationError]

## [0.0.3] - 26/09/2020
### Changed
- Modifica Dockerfile

## [0.0.2] - 23/09/2020
### Added
- Adiciona tela inicial
- Adiciona dependências python
- Adiciona lógica para gerar nuvem de palavras

## [0.0.1] - 21/09/2020
### Added
- Setup inicial
